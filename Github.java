package com.BlueOptima.Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Github {
	private String token;

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * A method which returns a list of Github Users having the desired
	 * parameters(name,location).This method searches users from
	 * (https://api.github.com/search/users). Returns empty list if no users found.
	 * 
	 */
	public List<GithubUser> searchByNameAndLocation(String firstName, String lastName, String location) {
		// validating inputs
		if (firstName == null)
			firstName = "";
		if (lastName == null)
			lastName = "";
		if (location == null)
			location = "";

		// Building URL String
		StringBuilder builder = new StringBuilder();
		String host = "https://api.github.com/search/users?";
		String fullname = firstName.toLowerCase().trim() + "%20" + lastName.toLowerCase().trim();
		builder.append(host).append("q=").append(fullname).append("+in:fullname+location:").append(location);

		System.out.println("=======");
		System.out.println("Searching for " + fullname + " from " + location);

		// getting HTTP connection
		List<GithubUser> listOfUsers = new ArrayList<GithubUser>();
		try {
			URL url = new URL(builder.toString());
			System.out.println(url);

			// Checking if querying UsersAPI is within limits
			while (!RateLimiter.queryUsersAPI()) {
				Thread.sleep(RateLimiter.timeLeft());
			}

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/vnd.github.v3+json");
			connection.setRequestProperty("Authorization", this.getToken());

			if (connection.getResponseCode() != 200 || connection == null) {
				throw new RuntimeException("Failed : HTTP Error code : " + connection.getResponseCode());
			}

			// Reading the output stream
			InputStreamReader in = new InputStreamReader(connection.getInputStream());
			BufferedReader br = new BufferedReader(in);
			String output;
			while ((output = br.readLine()) != null) {
				JsonObject jsonObject = new JsonParser().parse(output).getAsJsonObject();

				// checking if there is no match for the given parameters
				if (jsonObject.get("total_count").getAsInt() == 0)
					break;

				// Getting Jsonarray of valid users having same full name and location
				JsonArray userArray = jsonObject.getAsJsonArray("items");

				// Preparing the github user and adding it to listOfUsers
				for (JsonElement userJson : userArray) {
					GithubUser githubuser = getGithubuser(userJson);
					if (githubuser != null)
						listOfUsers.add(githubuser);
				}

			}

			connection.disconnect();

		} catch (Exception e) {
			System.out.println("Problem in getting list of users");
			System.out.println("========");
		}

		return listOfUsers;
	}

	/**
	 * A method which takes in the argument the Json representation of a user of
	 * github. This Json is deserialized as the instance of githubuser class.
	 * 
	 */

	public GithubUser getGithubuser(JsonElement userJson) {
		JsonObject user = (JsonObject) userJson;
		Gson gson = new Gson();

		// Deserializing userJson to an instance of GithubUser class
		GithubUser githubuser = gson.fromJson(user.toString(), GithubUser.class);

		// Getting array for repositories
		JsonArray reposArray = this.getReposJsonArray(user.get("repos_url").getAsString());
		List<Repository> listOfRepositories = new ArrayList<Repository>();
		// Accessing each repository from the reposArray
		if (reposArray != null) {
			for (JsonElement repositoryJson : reposArray) {
				Repository repository = gson.fromJson(repositoryJson.toString(), Repository.class);

				// Getting commits for the above repository
				String commitURL = repositoryJson.getAsJsonObject().get("commits_url").getAsString();
				int length = commitURL.length();
				commitURL = commitURL.substring(0, length - 6);
				JsonArray commitsArray = this.getCommitsJsonArray(commitURL);
				int numberOfCommits = 0;
				if (commitsArray != null) {
					numberOfCommits = commitsArray.size();
				}
				repository.setCommits(numberOfCommits);
				listOfRepositories.add(repository);

			}
		}

		githubuser.setRepositories(listOfRepositories);
		return githubuser;
	}

	/**
	 * This method takes the argument as the string representation of URL for
	 * retrieving repositories of a particular github user. As an output , it gives
	 * the Json Array of all the repositories of a user.It sends null if there are
	 * no repositories of a user.
	 */
	public JsonArray getReposJsonArray(String reposURL) {
		String output = null;
		JsonArray reposJsonArray = null;
		URL url;
		try {
			url = new URL(reposURL);
			System.out.println(url);

			// Checking if querying ReposAPI is within limits
			while (!RateLimiter.queryReposAPI()) {
				Thread.sleep(RateLimiter.timeLeft());
			}

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/vnd.github.v3+json");
			connection.setRequestProperty("Authorization", this.getToken());
			if (connection.getResponseCode() != 200 || connection == null) {
				try {
					throw new RuntimeException("Failed : HTTP Error code : " + connection.getResponseCode());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			InputStreamReader in = new InputStreamReader(connection.getInputStream());
			BufferedReader br = new BufferedReader(in);

			while ((output = br.readLine()) != null) {
				reposJsonArray = new JsonParser().parse(output).getAsJsonArray();
			}

			connection.disconnect();

		} catch (Exception e) {
			System.out.println("Not able to fetch repositories");
		}

		return reposJsonArray;
	}

	/**
	 * This method takes the argument as the string representation of URL for
	 * retrieving all the commits of a particular repository of a user. As output ,
	 * it sends the JsonArray comprising of information for every commit. It sends
	 * null if there are no commits.
	 */
	public JsonArray getCommitsJsonArray(String commitURL) {
		String output = null;
		JsonArray commitsJsonArray = null;
		URL url;

		try {
			url = new URL(commitURL);

			// Checking if querying CommitsAPI is within limits
			while (!RateLimiter.queryCommitsAPI()) {
				Thread.sleep(RateLimiter.timeLeft());
			}

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/vnd.github.v3+json");
			connection.setRequestProperty("Authorization", this.getToken());
			if (connection.getResponseCode() != 200 || connection == null) {
				try {
					throw new RuntimeException("Failed : HTTP Error code : " + connection.getResponseCode());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			InputStreamReader in = new InputStreamReader(connection.getInputStream());
			BufferedReader br = new BufferedReader(in);

			while ((output = br.readLine()) != null) {
				commitsJsonArray = new JsonParser().parse(output).getAsJsonArray();
			}
			connection.disconnect();
		} catch (Exception e) {
			System.out.println("Not able to fetch number of commits");
		}

		return commitsJsonArray;
	}

}

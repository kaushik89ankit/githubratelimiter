package com.BlueOptima.Main;

public class Repository {
	
	private String name;
	private int commits;
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[name=" + name + ", commits=" + commits + "]";
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the commits
	 */
	public int getCommits() {
		return commits;
	}
	/**
	 * @param commits the commits to set
	 */
	public void setCommits(int commits) {
		this.commits = commits;
	}
	
	
}	

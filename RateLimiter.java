package com.BlueOptima.Main;

import java.util.TimerTask;

public class RateLimiter extends TimerTask {

	static final int MAX_AUTHENTICATED_QUERY_LIMIT = 5000;
	static final int MAX_AUTHENTICATED_SEARCH_API_QUERY_LIMIT = 30;

	private static volatile int queriesLeftUsersAPI;
	private static volatile int totalqueriesleft;
	private static volatile int number_of_cycles = 0;

	public static synchronized boolean queryUsersAPI() {
		if (queriesLeftUsersAPI > 0) {
			queriesLeftUsersAPI--;
			totalqueriesleft--;

			return true;
		}
		return false;
	}

	public static synchronized boolean queryReposAPI() {
		if (totalqueriesleft > 0) {
			totalqueriesleft--;
			return true;
		}
		return false;
	}

	public static synchronized boolean queryCommitsAPI() {
		if (totalqueriesleft > 0) {
			totalqueriesleft--;
			return true;
		}
		return false;
	}

	// updating total queries to the max limit after one hour
	public static synchronized void updateTotalQueries() {
		totalqueriesleft = MAX_AUTHENTICATED_QUERY_LIMIT;
	}

	public static long timeLeft() {
		return 60000;
	}

	@Override
	public void run() {
		queriesLeftUsersAPI = MAX_AUTHENTICATED_SEARCH_API_QUERY_LIMIT;
		number_of_cycles++;

		// update after one hour
		if (number_of_cycles == 60) {
			number_of_cycles = 0;
			totalqueriesleft = MAX_AUTHENTICATED_QUERY_LIMIT;
		}

	}
}

package com.BlueOptima.Main;

import java.io.Serializable;
import java.util.List;

public class GithubUser implements Serializable{
	
	private String login;
	private String id;
	private String html_url;
	private String score;
	private List<Repository> repositories;
	
	
	
	/**
	 * @return the repositories
	 */
	public List<Repository> getRepositories() {
		return repositories;
	}
	/**
	 * @param repositories the repositories to set
	 */
	public void setRepositories(List<Repository> repositories) {
		this.repositories = repositories;
	}
	/**
	 * @return the login name
	 */
	public String getLoginName() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLoginName(String login) {
		this.login = login;
	}
	/**
	 * @return the id
	 */
	public String getUserId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setUserId(String id) {
		this.id = id;
	}
	/**
	 * @return the html_url
	 */
	public String getProfileURL() {
		return html_url;
	}
	/**
	 * @param html_url the html_url to set
	 */
	public void setProfileURL(String html_url) {
		this.html_url = html_url;
	}
	/**
	 * @return the score
	 */
	public String getScore() {
		return score;
	}
	/**
	 * @param score the score to set
	 */
	public void setScore(String score) {
		this.score = score;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GithubUser [loginName=" + login + ", UserID=" + id + ", ProfileURL=" + html_url + ", score=" + score + "\nRepositories :" + repositories  + "]";
	}
	
	// Getters & Setters
	
		
}

package com.BlueOptima.Main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Solution {

	public static void main(String[] args) throws InterruptedException {

		List<QueryParams> queryParamsList = new ArrayList<QueryParams>();

		try {

			BufferedReader reader = new BufferedReader(new FileReader(
					"/home/ankit/WorkSpaces/GitHubRateLimiter/GithubRateLimiter/src/main/java/com/BlueOptima/Main/FullnameLocation.csv"));
			String line = null;
			Scanner scanner = null;
			int index = 0;

			while ((line = reader.readLine()) != null) {
				QueryParams query = new QueryParams();
				scanner = new Scanner(line);
				scanner.useDelimiter(",");
				while (scanner.hasNext()) {
					String data = scanner.next();
					if (index == 0)
						query.setFirstName(data);
					else if (index == 1)
						query.setLastName(data);
					else if (index == 2)
						query.setLocation(data);

					index++;
				}
				index = 0;
				queryParamsList.add(query);
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Searching...");

		ExecutorService executor = Executors.newFixedThreadPool(4);
		Iterator<QueryParams> iterator = queryParamsList.iterator();
		Timer timer = new Timer();
		TimerTask task = new RateLimiter();
		timer.scheduleAtFixedRate(task, 0, 1000);

		while (iterator.hasNext()) {
			executor.execute(new Consumer(iterator.next()));
		}

		executor.shutdown();

		while (!executor.isTerminated()) {

		}
		System.out.println("Search ends !!");

		timer.cancel();
	}

}
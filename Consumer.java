package com.BlueOptima.Main;

import java.util.List;

public class Consumer implements Runnable {

	QueryParams query;
	String token = "c5ee6d4f7a4a111c000c68dafddbda379d407fa1";

	public Consumer(QueryParams query) {
		this.query = query;
	}

	public void run() {
		// Get list of github users with above name and location
		Github github = new Github();
		github.setToken(token);
		List<GithubUser> usersList = github.searchByNameAndLocation(query.firstName, query.lastName, query.location);
		if (usersList != null && usersList.size() != 0) {
			for (GithubUser user : usersList) {
				System.out.println(user);
			}

		}
	}

}
